.. what does a data science project look like
.. mix of research and software dev
.. so timelines exist, are flexible, and outcomes are uncertain (we can build a model, can't guarantee it will help)
.. lifecycle



.. revealjs:: Data Science Product Life Cycle
    :title-heading: h3


    .. revealjs:: Is this software development or research ?
        :title-heading: h6



        .. raw:: html

            <div class="fragment"> Yes </div>

        .. rv_note::

            Turns out its a little bit of both. The final product of a data science project consists of scientific software
            with varying levels of complexity (from simply report generation to full blown architectures).

            What does this mean for our customers ?

                - We can have and set deadlines to complete work, but must be ready to be flexible if we have to wait on data/computing
                    to finish

                - Outcomes are uncertain, we can build a model quickly. We can not guarantee the efficacy of that model

                - Research results have to be adopted by the business area. This can be anything from changing a process to implementing a
                    new system.



.. revealjs:: Design Thinking in Data Science
    :title-heading: h3


    .. revealjs::


      .. image:: _static/DataScienceProcess_bb.png
          :height: 500

      .. rv_note::

        Unsurprisingly this should closely follow the scientific method. We define the problem (and generate hypotheses) ,
            collect data and analyze it, and then evaluate our explanations (solutions). The added wrinkle here is that these
            observations need to be put into production and because our problems are dynamic and our customers needs change, we need to monitor solutions to
            ensure that they're performing adequately in the real world.



    .. revealjs:: Identify the problem
        :title-heading: h6

        Big enough to be valuable, but small enough to be doable.



        .. raw:: html

            <hr>
            <div class="fragment">Do : Make food more nutritious. </div>
            <div class="fragment">Don't : Solve world hunger. </div>



        .. rv_note::

            Always start with the problem. Figure out what you're trying to solve, don't worry about how. During this phase
            you may figure out that "data science" or "machine learning" aren't the solution, and that's fine as long as you're
            solving a valuable problem.

            What constitutes valuable ?

                - automating tasks -> reduction in manual labor / FTEs on a task (let your people do more valuable work)
                - increased accuracy -> fewer mistakes == fewer corrections (mis routed messages)
                - increased precision -> increased confidence == fewer missed shots (sales leads)
                - increased recall    -> a better net catches more fish (fraud)

            All of this can and should be measured via a KPI of some sort. We usually default to dollars saved because its easy but
            this is not a requirement.


    .. revealjs:: Understand and Process Data
        :title-heading: h6

        Catch it all.

        .. image:: _static/pokeball.gif

        |footnote|

        .. |footnote| raw:: html

           <p style="text-align:right;"> <font size="1"> Image Courtesy of <a href="https://www.deviantart.com/zel-duh/">Zel-Duh @ DeviantArt </a> </font> </p>

        .. rv_note::

            For any observation , you should identify any metric or instrumentation associated and try to find a way to bring it
            in.

            Understand that your data isn't perfect but it doesn't mean its not valuable. You can always add more/better instrumentation
            later.

            While historical data can be useful, its often much harder to cast a prediction far out in time. Metrics at or near an observed
            event will likely be more predictive (but maybe not as useful).

            Keep causality in mind. for predictive tasks, features should be collected PRIOR to an observation event.


    .. revealjs:: Analyze and Draw Conclusions
        :title-heading: h6

        .. raw:: html

            <div class="fragment"> Exploration </div>
            <div class="fragment"> Data cleaning </div>
            <div class="fragment"> Feature engineering </div>
            <div class="fragment"> Model building </div>
            <div class="fragment"> Model evaluation </div>

            <hr>

            <div class="fragment"> This is a cyclical process, during a single work cycle we may be able to iterate on it several times.</div>


    .. revealjs:: Communicate Results
        :title-heading: h6

        .. raw:: html

            <div class="fragment"> Does this proposed solution do better than what we currently have ?</div>
            <div class="fragment"> Can we implement the proposed solution ?</div>
            <hr>
            <div class="fragment"> This is when decisions are made to commit resources to change management and implementation.</div>



        .. rv_note::

            This was part of the modeling section, but its arguably the most important part of the modeling process. This activity
            is where we win or lose. Your data scientist should be confident in the performance and increased efficacy of their proposed
            solution.

            Better, better will entirely define on what you want to optimize and what your KPI is. Understanding how you measure performance
            for your project is absolutely critical. You should be wary of changing this metric w/o serious discussion.

            It is entirely possible that a solution can't be implemented. (Netflix competition years ago - great solution that                          received 1 million dollar prize. Never implemented because it wouldn't work at scale.)

            Peer review is important at this stage to ensure that comparision are as fair as possible and statistically valid.


    .. revealjs:: Take Action
        :title-heading: h6


        .. raw:: html

            <div class="fragment"> Pipelines </div>
            <div class="fragment"> Boilerplate </div>
            <div class="fragment"> Scaling </div>





        .. rv_note::

            We'll often model on a static CSV. real world implementation have to interact directly with data collection in
            a more realistic fashion.

            Boiler plate code that ensures the reliability of the model as a piece of software. ( type checking, graceful failure,
            exit, restart, logging, integration with other services, etc.)

            Code will run under different loads, being able to add/remove compute resources adaptively is crucial for resource management.


    .. revealjs:: Gather Feedback
        :title-heading: h6


        .. raw:: html

            <div class="fragment"> No software survives contact with a user. </div>
            <div class="fragment"> The world isn't static. Our model is.</div>


        .. rv_note::

            This is software. It will need maintenance sometimes.

            The model was trained on a particular "view" of the data. As time goes on the world changes. While the model might
            not be completely invalid, it may lose performance. We can track this and re train or update the model as needed.




