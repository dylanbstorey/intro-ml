

.. revealjs:: Before we start....
    :title-heading: h3

    .. revealjs:: Some common "complaints"

    .. revealjs:: I was promised <em>A.I.</em> !
        :title-heading: h6

        .. image:: _static/ai_vs_ml.png
            :height: 400


        .. rv_note::

            - Current AI is "just" the application of machine learning models to help machines behave intelligently.
            - ... and depending on who you're talking to, update themselves automatically.

            To be fair, automating a machine learning solution isn't trivial - but it also isn't as complex as a salesman
            will charge you for it.


    .. revealjs:: You're trying to take over a job/role !
        :title-heading: h6

        |auger_ref| vs |mech_ref| vs |drill_ref|

        .. |auger_ref| image:: _static/hand-auger.png
            :align: middle
            :width: 150px

        .. |mech_ref| image:: _static/mechanical_drill.png
                    :align: middle
                    :width: 150px


        .. |drill_ref| image:: _static/electric_drill.png
            :align: middle
            :width: 150px



        .. raw:: html

            <div class="fragment"> No - we're trying to make it easier. </div>
            <div class="fragment"> By making easy decisions automatic.</div>
            <div class="fragment"> Leaving you with the most valuable work. </div>



        .. rv_note::

            We hear this alot, current state of the art can do some pretty cool stuff but it is no where near ready to
            replace people.

            First weak vs. strong AI :
                - weak AI simulates human abilities automating time consuming (but trivial) tasks
                - strong AI simulates a humans intellectual capability.

            We're pretty good at easy (short term/lots of problems):
                - Is this spam ?
                - How much money should this cost ?

            Less good at harder problems:
                - Amazon using "ML" to go through resumes
                - Uber 2017 Phoenix, AI was essentially blind
                - Watson Oncology

            What we're better at :
                - Automating the "obvious solutions" (Is this a car ?)
                - Ranking lists based on the probability of an event (fraud, cold call sales)
                - Let's people work on edge cases (This is a car, but one that looks weird)
                - Let's people work on valuable cases (It takes hours to investigate fraud, so focus on things that might be)





