import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import imageio
import os


def predict(x,coefficients):
    """
    predict the y values given x values and co-efficients
    :param x:
    :param coefficients:
    :return:
    """

    y_pred = np.zeros(x.shape)
    for i in range(0,len(coefficients)):
        y_pred += coefficients[i] * (x ** i)
    return y_pred


def cost(x,y,coefficients):
    """
    calculate the cost between x and y
    :param x:
    :param y:
    :return:
    """

    y_pred = predict(x,coefficients)
    y_diff = y_pred - y

    return (1 / (2 * len(y))) * np.sum(y_diff ** 2)



def polynomial_fit(x,y,coefficients, lr=.00001,epochs=1):

    for i in range(epochs):
        y_pred = predict(x,coefficients)
        errors = y_pred - y
        coefficients -= lr * (1/len(y)) * np.dot(errors,x)
        c = cost(x,y,coefficients)

    return coefficients,c


def scatter_plot(x,y,name):
    """
    plot a scatter plot of x,y

    :param x:
    :param y:
    :param name:
    :return:
    """
    fig = plt.figure(figsize=(8, 6), dpi=80)
    ax = plt.axes()
    plt.xlabel('x')
    plt.ylabel('y')
    plt.scatter(x, y)
    plt.savefig('{}.png'.format(name))


def line_plot(y_act,x,y,name, error=None, errors=False, equation=None,epoch=None):
    """
    plot a line plot

    :param y_act: y used for fit
    :param x:
    :param y: y_pred
    :param name:
    :return:
    """
    fig = plt.figure(figsize=(8, 6), dpi=80)
    ax = plt.axes()
    plt.xlabel('x')
    plt.ylabel('y')
    plt.scatter(x, y_act)
    plt.plot(x, y, color='black')

    try :
        equation.any()
    except AttributeError:
        equation = np.zeros(0)

    if error:
        if error > 10000:
            plt.text(1, 100, 'error : Very High', color='black')
        else:
            plt.text(1, 100, 'error : {:.3f}'.format(error), color='black')
    if errors:
        plt.vlines(x, y_act, y, color='red')
    if len(equation) > 0:
        string = []
        for i,v in enumerate(equation):
            if i == 0:
                string.append('{:.3f}'.format(v))
            elif i == 1:
                string.append('{:.3f}x'.format(v))
            else:
                string.append('{:.3f}x^{}'.format(v,i))
        string.reverse()
        plt.text(1, 95, '{}'.format('${}$'.format('+'.join(string))))
    if epoch != None:
        plt.text(1, 90, 'epoch: {}'.format(epoch))


    name = '{}.png'.format(name)
    plt.savefig(name)
    return name



def model_update_animation(x, y):

    epochs = 125
    files = []
    f = 0
    #inital model settings
    coefficients = [0.1, 0.4, 0.6]
    error = cost(x,y,predict(x,coefficients))
    files.append(line_plot(y, x, predict(x, coefficients), f, error=error, equation=coefficients,epoch=0))
    f += 1
    files.append(line_plot(y, x, predict(x, coefficients), f, errors=True, error=error, equation=coefficients,epoch=0))
    f += 1

    for i in range(10):
        coefficients, error = polynomial_fit(x, y,coefficients , epochs=epochs)
        files.append(line_plot(y,x,predict(x,coefficients),f,error=error,equation=coefficients,epoch=i*epochs))
        f+=1
        files.append(line_plot(y, x, predict(x, coefficients), f, error=error,errors=True,equation=coefficients,epoch=i*epochs))
        f+=1


    with imageio.get_writer('poly_fit.gif', mode='I', duration=0.75) as writer:
        for f in files:
            image = imageio.imread(f)
            writer.append_data(image)
            os.unlink(f)



if __name__ == '__main__':
    np.random.seed(52)

    coefficients = [0,2,1] # 0 +  2 x + x^2
    n = 50
    x = np.random.random(n)
    x *= 10
    x.sort()
    y = predict(x,coefficients)
    delta = np.random.uniform(-10, 10, size=(n,))
    y+=delta

    scatter_plot(x,y,'scatter')

    l_coef, l_err = polynomial_fit(x,y,[0,4], epochs=10000)
    p_coef, p_err = polynomial_fit(x,y,[0,4,3], epochs=10000)


    line_plot(y,x,predict(x,l_coef), 'linear_fit')
    line_plot(y,x,predict(x,p_coef), 'polynomial_fit')

    line_plot(y,x,predict(x,l_coef), 'linear_fit_err',error=l_err)
    line_plot(y,x,predict(x,p_coef), 'polynomial_fit_err',error=p_err)

    model_update_animation(x,y)

