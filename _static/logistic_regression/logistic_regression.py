import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import imageio
import os




class LogisticRegression(object):
    """
    Homebrew logistic regression
    """

    def __init__(self, X, y):
        """
        Initialize the model with our dataset.

        :param X: features
        :param y: labels
        """

        self.X = X
        self.y = y

        # None <- initialized but not fit
        self.coefficients = None
        self.intercept = None

    def __append_intercept(self, X):
        """
        Add a constant for learning the intercept

        :param X:
        :return:
        """
        intercept = np.ones((X.shape[0], 1))
        return np.concatenate((intercept, X), axis=1)

    def fit(self, epochs, lr=0.001, intercept=True):
        """
        Fit our model to the data

        :param epochs:
        :param lr:
        :param intercept:
        :return:
        """

        # A local copy for mangling intercepts


        # The first time fit is called, see if we need want to add an intercept
        if self.intercept is None:
            self.intercept = intercept
            if self.intercept:
                self.X = self.__append_intercept(self.X)

        # The first time fit is called initialize co-efficients.
        if self.coefficients is None:
            self.coefficients = np.zeros(self.X.shape[1])





        for i in range(epochs):
            error = self.y - self.predict(self.X,intercept_added=True)
            grad = np.dot(self.X.T, error)
            self.coefficients += lr * grad

        return self.coefficients, self.cost(self.X,self.y,intercept_added=True)



    def predict(self, X, intercept_added=False):
        """
        Predict based on current coefficients of the model

        :param X:
        :return:
        """

        if not intercept_added:
            if self.intercept:
                X = self.__append_intercept(X)

        return 1 / (1 + np.exp(-1 * np.dot(X, self.coefficients)))

    def cost(self, X, y, **kwargs):
        """

        :param X: predicted values
        :param y: actual values
        :return:
        """

        return np.sum(y * self.predict(X,**kwargs) - np.log(1 + np.exp(self.predict(X,**kwargs))))

    def scatter_plot(self, name, line = False):
        """
        plot a scatter plot of x,y, colored by label

        :param x:
        :param y:
        :param name:
        :return:
        """
        fig = plt.figure(figsize=(8, 6), dpi=80)
        ax = plt.axes()

        if self.intercept:
            plt.scatter(self.X[:, 1], self.X[:, 2], c=self.y, alpha=0.5, cmap='RdBu')
        else:
            plt.scatter(self.X[:, 0], self.X[:, 1], c=self.y, alpha=0.5, cmap='RdBu')

        if line:
            (x_min, x_max) = ax.get_xlim()
            x = np.linspace(x_min, x_max, 100)
            y = -(self.coefficients[0] + self.coefficients[1] * x) / self.coefficients[2]
            plt.plot(x, y, color='black', alpha=.9)

        plt.savefig('{}.png'.format(name))

    def decision_plot(self,X,y,name,error=None,epoch=None):
        """
        Plot our learned line
        :param x:
        :param coefficients:
        :return:
        """

        fig, ax = plt.subplots(figsize=(8, 6), dpi=80)

        if self.intercept:
            plt.scatter(self.X[:, 1], self.X[:, 2], c=self.y, alpha=0.5, cmap='RdBu',edgecolor='white')
        else:
            plt.scatter(self.X[:, 0], self.X[:, 1], c=self.y, alpha=0.5, cmap='RdBu',edgecolor='white')

        (y_min, y_max) = ax.get_ylim()
        (x_min, x_max) = ax.get_xlim()



        xx, yy = np.mgrid[x_min:x_max:.01, y_min:y_max:.01]

        grid = np.c_[xx.ravel(), yy.ravel()]
        probabilities = self.predict(grid).reshape(xx.shape)


        contour = ax.contourf(xx, yy, probabilities, 25, cmap='RdBu', vmin=0, vmax=1,alpha=0.6)

        ax_c = fig.colorbar(contour)
        ax_c.set_label("$P(blue)$")
        ax_c.set_ticks([0, .25, .5, .75, 1])

        if self.intercept:
            plt.scatter(self.X[:, 1], self.X[:, 2], c=self.y, alpha=0.25, cmap='RdBu',edgecolor='white')
        else:
            plt.scatter(self.X[:, 0], self.X[:, 1], c=self.y, alpha=0.25, cmap='RdBu',edgecolor='white')



        if error:
            plt.text(.1,.9, 'error: {:.2f}'.format(error),color='white',transform=ax.transAxes)
        if epoch:
            plt.text(.1, .87, 'epoch: {:.2f}'.format(epoch), color='white', transform=ax.transAxes)

        x = np.linspace(x_min, x_max, 100)
        y = -(self.coefficients[0] + self.coefficients[1] * x) / self.coefficients[2]
        plt.plot(x, y, color='black', alpha=.9)

        name = '{}.png'.format(name)
        plt.savefig(name)
        return name


if __name__ == '__main__':
    np.random.seed(52)
    observations = 150

    x1 = np.random.multivariate_normal([0, 0], [[.5, .25], [.25, .5]], observations)
    x2 = np.random.multivariate_normal([1, 4], [[.5, .25], [.25, .5]], observations)

    features = np.vstack((x1, x2)).astype(np.float32)
    labels = np.hstack((np.zeros(observations),
                        np.ones(observations)))



    model = LogisticRegression(X=features,y=labels)


    model.scatter_plot('initial')


    with imageio.get_writer('logi_fit.gif', mode='I', duration=0.75) as writer:
        for x in range(0,100,5):
            coefficients, cost = model.fit(epochs=x)
            name = model.decision_plot(model.X,model.y, x ,error=cost,epoch=x)
            image = imageio.imread(name)
            writer.append_data(image)
            os.unlink(name)


    model.scatter_plot('final',line=True)


    # decision_plot(features, coefficients, 'test')
