import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import imageio
import os





def linear_regression(X, y, m_current=0, b_current=0, epochs=10, learning_rate=1e-3):
    """

    Fit a linear line to set of points

    :param X:
    :param y:
    :param m_current: current slope of the line
    :param b_current: current intercept
    :param epochs:    number of epochs to use in fitting
    :param learning_rate: learning rate
    :return: updated m(slope), b(intercept), cost(MSE)
    """

    N = float(len(y))

    for i in range(int(epochs)):
        y_pred = (m_current * X) + b_current  # fit data with current models

        diff = y-y_pred
        cost = (diff**2).sum() / N
        m_gradient = -(2 / N) * (X * diff).sum()
        b_gradient = -(2 / N) * diff.sum()
        m_current = m_current - (learning_rate * m_gradient)
        b_current = b_current - (learning_rate * b_gradient)
    return m_current, b_current, cost







def model_update_visualization(x,y):
    """
    creates our visualization(s) for what ml looks like conceptually
    :param epochs:
    :return:
    """

    #Scatter plot of data

    fig = plt.figure(figsize=(8, 6), dpi=80)
    ax = plt.axes()
    plt.xlabel('x')
    plt.ylabel('y')
    plt.scatter(x,y)
    plt.savefig('scatter.png')

    #First plot , starting position
    fig = plt.figure(figsize=(8, 6), dpi=80)
    ax = plt.axes()
    plt.xlabel('x')
    plt.ylabel('y')
    plt.scatter(x,y)
    plt.plot(x, 0*x + 0,color='black')
    plt.savefig('initial_model.png')

    fig = plt.figure(figsize=(8, 6), dpi=80)
    ax = plt.axes()
    plt.xlabel('x')
    plt.ylabel('y')
    plt.scatter(x, y)
    plt.plot(x, 0 * x + 0, color='black')
    plt.vlines(x, y, 0 * x + 0, color='red')
    error = y - (0 * x + 0)
    error = (error ** 2).sum() / len(y)
    plt.text(1, 18, 'error : {:.3f}'.format(error), color='red')
    plt.savefig('initial_error.png')

    #do a few updates and save outputs
    fig = plt.figure(figsize=(8, 6), dpi=80)
    ax = plt.axes()
    plt.xlabel('x')
    plt.ylabel('y')
    m, b, cost = linear_regression(x, y, 0, 0,epochs=5)
    print(m, b, cost)
    plt.scatter(x,y)
    plt.plot(x, m * x + b,color='black')
    plt.savefig('second_model.png')

    fig = plt.figure(figsize=(8, 6), dpi=80)
    ax = plt.axes()
    plt.xlabel('x')
    plt.ylabel('y')
    plt.scatter(x, y)
    plt.plot(x, m * x + b, color='black')
    plt.vlines(x, y, m * x + b, color='red')
    error = y - (m * x + b)
    error = (error ** 2).sum() / len(y)
    plt.text(1, 18, 'error : {:.3f}'.format(error), color='red')
    plt.savefig('second_error.png')

    #do a few updates and save outputs
    fig = plt.figure(figsize=(8, 6), dpi=80)
    ax = plt.axes()
    plt.xlabel('x')
    plt.ylabel('y')
    m, b, cost = linear_regression(x, y, m, b,epochs=5)
    print(m,b,cost)
    plt.scatter(x,y)
    plt.plot(x, m * x + b,color='black')
    plt.savefig('third_model.png')

    fig = plt.figure(figsize=(8, 6), dpi=80)
    ax = plt.axes()
    plt.xlabel('x')
    plt.ylabel('y')
    plt.scatter(x, y)
    plt.plot(x, m * x + b, color='black')
    plt.vlines(x, y, m * x + b, color='red')
    error = y - (m * x + b)
    error = (error ** 2).sum() / len(y)
    plt.text(1, 18, 'error : {:.3f}'.format(error), color='red')
    plt.savefig('third_error.png')

    #do a few updates and save outputs
    fig = plt.figure(figsize=(8, 6), dpi=80)
    ax = plt.axes()
    plt.xlabel('x')
    plt.ylabel('y')
    m, b, cost = linear_regression(x, y, m, b,epochs=20)
    print(m,b,cost)
    plt.scatter(x,y)
    plt.plot(x, m * x + b,color='black')
    plt.savefig('fourth_model.png')

    fig = plt.figure(figsize=(8, 6), dpi=80)
    ax = plt.axes()
    plt.xlabel('x')
    plt.ylabel('y')
    plt.scatter(x, y)
    plt.plot(x, m * x + b, color='black')
    plt.vlines(x, y, m * x + b, color='red')
    error = y - (m * x + b)
    error = (error ** 2).sum() / len(y)
    plt.text(1, 18, 'error : {:.3f}'.format(error), color='red')
    plt.savefig('fourth_error.png')

    #Which model is better ?
    fig = plt.figure(figsize=(8, 6), dpi=80)
    ax = plt.axes()
    plt.xlabel('x')
    plt.ylabel('y')
    plt.scatter(x,y)
    plt.plot(x, 2 * x + -1, color='red')
    plt.plot(x, 2 * x + 0, color='orange')
    plt.plot(x, 2 * x + 1, color='green')
    plt.savefig('which_model.png')

    #Which error ?
    #Which model is better ?
    fig = plt.figure(figsize=(8, 6), dpi=80)
    ax = plt.axes()
    plt.xlabel('x')
    plt.ylabel('y')
    plt.scatter(x,y)
    plt.plot(x, 2 * x + -1, color='red')
    plt.plot(x, 2 * x + 0, color='orange')
    plt.plot(x, 2 * x + 1, color='green')

    error_r = y - (2 * x + -1)
    error_r = (error_r ** 2).sum() / len(y)
    plt.text(1, 20, 'error : {:.3f}'.format(error_r), color='red')

    error_g = y - (2 * x + 1)
    error_g = (error_g ** 2).sum() / len(y)
    plt.text(1, 18, 'error : {:.3f}'.format(error_g), color='green')

    error_o = y - (2 * x + 0)
    error_o = (error_o ** 2).sum() / len(y)
    plt.text(1, 16, 'error : {:.3f}'.format(error_o), color='orange')

    plt.savefig('which_error.png')

    ##Error
    fig = plt.figure(figsize=(8, 6), dpi=80)
    ax = plt.axes()
    plt.xlabel('x')
    plt.ylabel('y')
    plt.scatter(x, y)
    plt.plot(x, m * x + b, color='black')
    plt.vlines(x,y, m * x + b, color='red')
    error = y - ( m*x + b)
    error = (error ** 2).sum() / len(y)
    plt.text(1, 18, 'error : {:.3f}'.format(error), color='red')
    plt.savefig('error.png')

    pass


def error_plot(x, y, m, b, epoch, name, error):
    """
    create an error plot of a model
    :param x: X values
    :param y: Y Values
    :param m: slope of the model
    :param b: intercept
    :param epoch: which epoch
    :param name: file name to save as
    :param error: model error
    :return:
    """

    # Fresh Image
    fig = plt.figure(figsize=(8, 6), dpi=80)
    ax = plt.axes()
    plt.xlabel('x')
    plt.ylabel('y')
    plt.text(1, 22, 'round {}'.format(epoch))
    plt.text(1, 20, '{:.3f}x+{:.3f}'.format(m, b))
    plt.text(1, 18, 'error : {:.3f}'.format(error), color ='red')
    # Scatter
    plt.scatter(x, y)
    # Current Model
    plt.plot(x, m * x + b, color='black')
    # Error
    plt.vlines(x, y, m * x + b, color='red')
    file = '{}.png'.format(name)
    plt.savefig(file)
    return file

def line_plot(x, y, m, b, epoch, name, error):
    """
    Plot a single line
    :param x:
    :param y:
    :param m:
    :param b:
    :param epoch:
    :param name:
    :return:
    """
    # Fresh Image
    fig = plt.figure(figsize=(8, 6), dpi=80)
    ax = plt.axes()
    plt.xlabel('x')
    plt.ylabel('y')
    plt.text(1, 22, 'round {}'.format(epoch))
    plt.text(1, 20, '{:.3f}x+{:.3f}'.format(m, b))
    # Scatter
    plt.scatter(x, y)
    # Current Model
    plt.plot(x, m * x + b, color='black')
    file = '{}.png'.format(name)
    plt.savefig(file)
    return file


def shifting_line_plot(x, y, m, b, m_prev, b_prev, epoch, name, error):
    # Fresh Image
    fig = plt.figure(figsize=(8, 6), dpi=80)
    ax = plt.axes()
    plt.xlabel('x')
    plt.ylabel('y')
    plt.text(1, 22, 'round {}'.format(epoch))
    plt.text(1, 20, '{:.3f}x+{:.3f}'.format(m, b))
    plt.text(1, 18, 'error : {:.3f}'.format(error), color='black')
    # Scatter
    plt.scatter(x, y)
    # Prev Model
    plt.plot(x, m_prev * x + b_prev, color='gray')
    # Current Model
    plt.plot(x, m * x + b, color='black')
    file = '{}.png'.format(name)
    plt.savefig(file)
    return file

def model_update_animation(x, y):

    #containers
    f = 0
    files = []
    epochs = 7


    #inital model settings
    m = 0
    b = 0
    y_pred = np.zeros(len(y))
    diff = y - y_pred
    cost = (diff ** 2).sum() / len(y)

    for i in range(8):

        files.append(line_plot(x, y, m, b, i * epochs, f, cost))
        f+=1
        files.append(error_plot(x, y, m, b, i * epochs, f, cost))
        f+=1

        m_prev = m
        b_prev = b
        cost_prev = cost
        m,b,cost = linear_regression(x, y, m, b, epochs=epochs)

        files.append(shifting_line_plot(x, y, m, b, m_prev, b_prev, i * epochs, f, cost_prev))
        f+=1

        print(m, b, cost)

    with imageio.get_writer('regression.gif', mode='I', duration=0.5) as writer:
        for f in files:
            image = imageio.imread(f)
            writer.append_data(image)
            os.unlink(f)

    pass


def main():
    np.random.seed(seed=1337)
    x = np.random.random(50) * 10
    delta = np.random.uniform(-5, 5, size=(50,))
    y = 2 * x + 0.1
    y += delta

    model_update_visualization(x,y)
    model_update_animation(x,y)

if __name__ == '__main__':
    main()


