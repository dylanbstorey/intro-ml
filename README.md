## Intro to Data Science/Machine Learning


This is a presentation that I use and have used to help business partners understand what data science
projects and how they're created and an managed.

This is done at an incredibly high level and is technically inaccurate at a few spots in order to 
gloss over technical nuance in favor of understanding.


Use the following keyboard keys to interact with the presentation



-  `<Esc>` pull up an arial view of the presentation
- `<Space>` next slide
- `<arrows>` navigate slides in direction
- `<s>` speaker notes