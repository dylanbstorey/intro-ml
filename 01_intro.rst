.. revealjs::

  .. revealjs:: Intro to Data Science/Machine Learning
    :data-background: _static/logo.png
    :data-background-size: 50%


    |block_quote|


    .. |block_quote| raw:: html

      <blockquote>
      &ldquo;Learning to think like a machine thinks about learning.&rdquo;
      </blockquote>


  .. revealjs:: Intended Audience
      :title-heading: h3

      - People managing business areas who are interested in leveraging data science
      - People who aren't sure what is hype and what isn't
      - People with limited/no exposure to research oriented projects

  .. revealjs:: Goals:
      :title-heading: h3

      - Briefly introducte *data science*
      - Explain how *machine learning* works
      - Explain the *product cycle* of a data science solution
      - Understand what makes *deep learning* special



      |promise|

      .. |promise| raw:: html

          <blockquote>
              &ldquo;Without any Greek notation!*&rdquo;
          </blockquote>
                <div align="right" class="fragment">
                <small> *For some definitions of without. </small>
                </div>

      .. rv_note::

            When you leave you should be able to...

            ...and we'll attempt to prep you for this without greek notation. (some arithmetic and multiplication will be
            required).






