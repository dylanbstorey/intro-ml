.. revealjs::

    .. revealjs:: What is Data Science ?
      :title-heading: h3


      |ds_block_quote|


    .. |ds_block_quote|  raw:: html

      <blockquote>
        Data science is a multi disciplinary field that uses scientific methods, processes, algorithms and systems to extract knowledge  and insights from structured and unstructured data.
      </blockquote>


    .. rv_note::

         This is one of the best academic definitions I've seen, it also is so generic that it defines any scientific
         domain. It is useful though in that it does a good job emphasizing science over data/technology.

         The biggest issue with it is that it emphasizes knowledge as a product.




    .. revealjs:: A tighter definition

        |ds_block_quote_2|



    .. |ds_block_quote_2| raw:: html

       <blockquote>
          The application of scientific methods to data in order to derive value for the business.
       </blockquote>



    .. rv_note::

       Aquiring knowledge is a laudable goal, but the application of knowledge in order to provide value is critical in order to sustain a practice.

       This definition allows us to be confident that the answers provided are good and rigourous, but the outcome must be applied and provide value.

       The destination matter as much as he journey.



