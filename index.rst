=================================================
Intro to Data Science/Machine Learning
=================================================


.. include:: 01_intro.rst
.. include:: 02_accusation_audit.rst
.. include:: 03_what_is_data_science.rst
.. include:: 03_what_is_machine_learning.rst
.. include:: 04_how_to_learn.rst
.. include:: 05_deep_learning.rst
.. include:: 06_data_science_product_cycle.rst
.. include:: 07_outtro.rst