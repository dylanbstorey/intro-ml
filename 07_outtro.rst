.. revealjs::
      :data-background: _static/logo.png
      :data-background-size: 50%


      .. revealjs:: Learnings:
            :title-heading: h3


            - *Data Science* is the application of scientific methods in order to derive value to the business.
            - *Machine learning* is just really fancy guessing until we learn rules to be correct.
            - *Deep learning* is just complex math that frees us from certain constraints.
            - Data science is both *research* and *software development*.



