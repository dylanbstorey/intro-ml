
.. revealjs:: Deep Learning
    :title-heading: h3

    .. revealjs:: Why are we talking about this ?
       :title-heading: h6


       For a few reasons :

       - It's popular
       - Some of the names make it sound daunting.
       - It's powerful
       - It's largely just linear/logistic regression - just more of it.


    .. revealjs:: The Neuron
        :title-heading: h6


        .. image:: _static/base_perceptron_animated.gif
            :height: 400

        .. rv_note::

            The neuron in a neural network consists of two things :

                - A summation function
                - An activation function

            The linear function takes a series of inputs, multiplies each
            by a specific weight, adds them together.

            Then applies an "activation function" that determines whether/how much
            of that value should get passed on.


    .. revealjs:: The Network
        :title-heading: h6


        .. image:: _static/minimum_nn.png
            :height: 400



        .. rv_note::

            A network is then made up of many of these neurons. Stacking them together
            into layers, each layer talking to the layer before it.


    .. revealjs:: Learning on the Network
        :title-heading: h6

        .. image:: _static/nn_training.gif
            :height: 400



    .. revealjs:: Learning on the Network
        :title-heading: h6

        .. image:: _static/nn_learns_weights.gif


        .. rv_note::

            In a network the parameters are the weights and biases that are added to each
            neuron. Learning is much the same as we see in linear regression, we start with
            some guessed weights, see if they work, and change them to get better results.


    .. revealjs:: Bigger
        :title-heading: h6

        .. image:: _static/res-net-50.png
            :height: 300


        .. rv_note::

            Simple networks are great, but to tackle real world problems we often have to add many extra layers and types of connections in to learn.


    .. revealjs:: Different Connections
        :title-heading: h6


        .. image:: _static/so_many_choices.png
            :height: 400


.. revealjs:: Why do all of this extra work?
    :title-heading: h3


    .. revealjs::


        .. rv_note::

            This seems like a whole lot of work and complication, why do it, and what do we gain.




    .. revealjs:: Feature Engineering
        :title-heading: h6



        .. rv_note::

            A brief side track.

            Feature engineering is an integral part of the machine learning process.
            In it's essence it is the process by which we ask the following question : Given the data I have access to - how can i augment it to gain more information about my observation.



    .. revealjs:: What makes a dog a dog ?
        :title-heading: h6


        .. image:: _static/dog.jpg



        .. rv_note::

            Let's say I need to build an algorithm that detects that a dog is in this picture.
            What are some features I might engineer to help my algorithm learn better ?

                - ears
                - eyes
                - color
                - shape
                - etc.


    .. revealjs:: What does deep learning do ?
        :title-heading: h3

        .. image:: _static/dog_edges.png

        .. rv_note::

            It learns filters that help extract certain portions of the image that get
            us to "what makes up a dog".


    .. revealjs:: We now have two options with very different costs/benefits
        :title-heading: h3

        .. raw:: html

            <div class="fragment"> Have your people do feature engineering using their personal knowledge.</div>
            <div class="fragment"> Have computers do the feature engineering for you.</div>


        .. rv_note::

            You can have your people spend time being creative and thinking up new ways of
            combining and augmenting your dataset to make it better for the task.

            Pros : your people probably have a better intuitive understanding and may be able to make connections that aren't obvious quickly.

            Cons : Expensive, non automatable, not guaranteed to be useful.


            You can have a computer do it.

            Pros: Absolutely automatable, your people can go do other things while the computer figures it out.
            Almost gauranteed to come up with something that works.

            Cons: Features while explainable, may lose the intuitive part that makes them understandable to everyone.


