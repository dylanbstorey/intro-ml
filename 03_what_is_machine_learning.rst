.. revealjs::

    .. revealjs:: What <em>is</em> machine learning ?
        :title-heading: h3

        What it isn't :

        .. raw:: html


         <div class="fragment"> a black box<div>
         <div class="fragment"> a mechanism that predicts the future<div>
         <div class="fragment"> intelligent <div>


        .. rv_note::

         Take a page from Dr. Suess here and tell you what its not.

         - We can treat these processes as black boxes but are fully capable of looking into and understanding these models behaviors
         - ML doesn't really predict the future, it makes a decent guess based on current and past behaviour to what is going to
           happen in the near term. This is only true as long as the past and future are very similar.
         - ML may appear intelligent, but only in the sense that an axe is. Its a highly specialized tool that needs humans
           to leverage it well to make it useful and keep it sharp.



    .. revealjs:: So ... what is it then ?
        :title-heading: h3

        A work horse of most data science projects.

        Which is why we're dedicating a whole section to it.

    .. revealjs:: Conventional Programming
        :title-heading: h3

        Give a computer instructions on how to behave and make every decision.

        .. image:: _static/even_odd.gif
            :height: 400

        .. rv_note::

          In conventional programming we explicitly tell a computer how to process data and make decisions.


    .. revealjs:: Machine Learning
        :title-heading: h3

        .. image:: _static/xkcd_ml.png
            :height: 400

        .. raw:: html


            <div class="fragment"> Give computer input data</div>
            <div class="fragment"> Give a computer desired output</div>
            <div class="fragment"> Let the computer figure how to get output from input</div>


        .. rv_note::

          In machine learning, we give an algorithm a set of observations and demonstrated outcomes. And the have it figure out how to
          predict an outcome based on those observations, so that in the future we'll know whats going to happen before it happens.

    .. revealjs:: Formal Definition
        :title-heading: h3

                An intersection of mathematics & computer science by which we can discover patterns and learn some mapping function between input data and desired outcomes.

