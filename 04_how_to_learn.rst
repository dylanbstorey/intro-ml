.. revealjs:: How does a machine learn ?
    :title-heading: h3

    .. revealjs:: Experiential Learning
        :title-heading: h6

        .. raw:: html

            The best way to learn to cook is ...  to go cook. <br> <hr>



            <div class="fragment"> You'll cook some bad meals</div>
            <div class="fragment"> That you still have to eat</div>
            <div class="fragment"> So you change how you cook </div>
            <div class="fragment"> Until you're cooking good meals</div>


    .. rv_note::

        Let the computer figure it out is trivializing what happens - so lets talk about how learning happens and then how a computer learns.

        A caveat, people can go take classes and learn about how to cook specific meals etc, but at the end of the day the act
        of doing along with feedback is where people truly learn and internalize the knowledge

    .. revealjs:: Experiential Learning
        :title-heading: h6

        .. raw:: html

            The best way to learn to do <em>X</em>  is ... to go try to do <em>X</em>. <br> <hr>


            <div class="fragment"> You'll make mistakes</div>
            <div class="fragment"> That you will be punished for</div>
            <div class="fragment"> So you change how you do X </div>
            <div class="fragment"> Until you can do X</div>


    .. rv_note::

        Let's generalize this process.

        This is pretty much exactly how machine learning works.



    .. revealjs:: How do you punish a computer ?
        :title-heading: h6


        |office_ref|

        .. |office_ref| image:: _static/office_space.gif

        .. rv_note:

            While cathartic, this is probably not the best way to get a computer to adopt new behaviors.


    .. revealjs::


        You punish computers the same way you do people ...

        .. rst-class:: fragment roll-in

            with math !





    .. revealjs::

        You want to be able to predict the gallons of water required (:math:`y`) for total number of plants (:math:`x`) in your garden.

        .. image:: _static/linear_regression/scatter.png
            :height: 400

        .. rv_note::

            Let's start with a simple example.

            So you went and collected data from your neighbors on how many plants they had in there garden and how many
            gallons of water they consumed each month.



    .. revealjs::
        :title-heading: h6



        .. image:: _static/linear_regression/initial_model.png
            :height: 400

        .. rv_note::

            Your first model looks like this. Do you think it could be better ? How would you move the line ?

            ( In this picture m = 0 and b = 0 )

    .. revealjs::
        :title-heading: h6


        .. image:: _static/linear_regression/second_model.png
            :height: 400

        .. rv_note::

            Hopefully in this general direction (you'd increase m)

    .. revealjs::
        :title-heading: h6


        .. image:: _static/linear_regression/third_model.png
            :height: 400

        .. rv_note::

            More.

    .. revealjs::
        :title-heading: h6


        .. image:: _static/linear_regression/fourth_model.png
            :height: 400

        .. rv_note::

            Cool. But how do you know you're done ?

    .. revealjs::


        Which is best ?

        .. image:: _static/linear_regression/which_model.png
            :height: 400


    .. revealjs::

        How can you tell ?

        .. rst-class:: fragment

            We use an error function.



    .. revealjs:: Mean Squared Error
        :title-heading: h6

        **Given :**

        - True Values of : 1, 10, 4, 5
        - Predicted Values of : 3, 9, 5, 6


        .. rst-class:: fragment

           **MSE:**

           .. math::

             \frac{ ( 1 - 3 )^2 + (10 - 9)^2 + (4 - 5)^2 + (5 - 6)^2} { 4 }

        .. rv_note::

            This is the first math of the slide. Its mean squared error. You take the difference of the predicted vs true
            values, square it, add it all together, and divide by the number of values.

            There are MANY other Error functions for different scenarios. This ones common to use though. It tells you for a
            given model that makes prediction how wrong you'll be on average.

    .. revealjs::

        .. image:: _static/linear_regression/initial_error.png
            :align: left
            :height: 225

        .. image:: _static/linear_regression/second_error.png
            :align: right
            :height: 225

        .. raw:: html

            <br>

        .. image:: _static/linear_regression/third_error.png
            :align: left
            :height: 225

        .. image:: _static/linear_regression/fourth_error.png
            :align: right
            :height: 225


        .. rv_note::

            As we we're updating model , you can see that we were decreasing our error.

    .. revealjs::

        .. image:: _static/linear_regression/which_error.png


        .. rv_note::

            And now we can make hard decisions about things that visually looked very similar.


    .. revealjs:: Linear Regression
        :title-heading: h3

        You want to learn the function for the line (:math:`y=mx+b`) that best fits this data.

        .. raw:: html

            <ul>
            <li class="fragment roll-in"> Guess values of <math><mi>m</mi></math> and                       <math><mi>b</mi></math></li>
            <li class="fragment roll-in"> Figure out how wrong you are.</li>
            <li class="fragment roll-in"> Change the values of <math><mi>m</mi></math> and                  <math><mi>b</mi></math> </li>
            <li class="fragment roll-in"> Until you don't get any better</li>
            </ul>


    .. revealjs:: Linear Regression
        :title-heading: h6

        You want to learn the function for the line (:math:`y=mx+b`) that best fits this data.



        .. image:: _static/linear_regression/regression.gif
            :height: 300


        .. rv_note::

            ALL machine learning operates under this very basic idea. We just generally have many more functions than simple
            lines that we want to try and fit.


    .. revealjs:: What about ...
        :title-heading: h6




        .. image:: _static/polynomial_regression/scatter.png
            :height: 400


        .. rv_note::

            What about this line, you think we could learn this ?


    .. revealjs:: Which do you prefer ...
        :title-heading: h6


        .. image:: _static/polynomial_regression/linear_fit.png
            :align: left
            :height: 300

        .. image:: _static/polynomial_regression/polynomial_fit.png
            :align: right
            :height: 300


    .. revealjs:: Which do you prefer ...
        :title-heading: h6


        .. image:: _static/polynomial_regression/linear_fit_err.png
            :align: left
            :height: 300

        .. image:: _static/polynomial_regression/polynomial_fit_err.png
            :align: right
            :height: 300

        .. rv_note::

            Hopefully someone asks about the error function !


    .. revealjs::

        .. image:: _static/polynomial_regression/poly_fit.gif


    .. revealjs:: What about Classification ?
        :title-heading: h6

        Given observational data on location of house (:math:`x` , :math:`y`) predict what sports team they support (:math:`red` vs :math:`blue`).


        .. image:: _static/logistic_regression/initial.png
            :align: center
            :height: 300


    .. revealjs:: Attempt to find a line that best separates two groups of dots.
        :title-heading: h6

        .. image:: _static/logistic_regression/final.png
            :align: center
            :height: 300

    .. revealjs::

        .. image:: _static/logistic_regression/logi_fit.gif

.. revealjs:: Requirements for Machine Learning
    :title-heading: h3

    .. raw:: html

        <ul>
        <li class="fragment roll-in"> A target variable </li>
        <li class="fragment roll-in"> Observational data </li>
        <li class="fragment roll-in"> An error function  </li>
        </ul>


    .. rv_note::

        - something you'd like to predict
        - observations you think control the outcome of your target
        - how you want to measure performance


        For any ML/AI project these are the absolute requirements. From the simplest excel spreadsheet to the most advanced
        image/speech recognition problems we're working on today.
